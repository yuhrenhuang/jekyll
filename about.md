---
layout: page
title: About
permalink: /about/
---

This is the base Jekyll theme. You can find out more info about customizing your Jekyll theme, as well as basic Jekyll usage documentation at [jekyllrb.com](http://jekyllrb.com/)

You can find the source code for the Jekyll new theme at:
{% include icon-github.html username="jglovier" %} /
[jekyll-new](https://github.com/jglovier/jekyll-new)

You can find the source code for Jekyll at
{% include icon-github.html username="jekyll" %} /
[jekyll](https://github.com/jekyll/jekyll)

<ol class="detail-toc">
            

  <li class="toc-level-1 t-toc-level-1">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320983.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">主題演講</font></font></a>
      
    
    
      <ol>
        

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320983.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">修復JavaScript日期：從明尼阿波利斯到微軟，TC39以及兩者之間的</font></font></a>
      
      <font style="vertical-align: inherit;"><span class="chapter-running-time"><font style="vertical-align: inherit;">旅程--Maggie </font></span><a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320983.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;">Pint（微軟）</font></a></font><span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:12:42</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321020.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">什麼在AWS廚房烹飪？</font><font style="vertical-align: inherit;">更好的網絡食譜（由亞馬遜贊助）Cherie Wong（亞馬遜）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:07:59</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320986.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">瀏覽器的平行未來 - </font></font></a>
      
      <font style="vertical-align: inherit;"><span class="chapter-running-time"><font style="vertical-align: inherit;">林克拉克</font></span><a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320986.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;">（Mozilla）</font></a></font><span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:14:45</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320982.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">它的拼寫是“可訪問性”，而不是“殘疾” - 斯科特戴維斯（ThoughtWorks）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:14:39</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320990.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">配置的自由是建立一個更美好世界的自由。</font><font style="vertical-align: inherit;">-  Cory Doctorow（EFF）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:19:02</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320977.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">與Cory Doctorow的問答環節 -  Cory Doctorow（EFF）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:16:48</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320987.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">你在這？</font><font style="vertical-align: inherit;">地理空間網站發布了人跡罕至的地圖 -  Aurelia Moser（Mozilla Science）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:19:13</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320975.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">JavaScript的成本 -  Addy Osmani（谷歌）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:20:07</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321005.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可以開源改變比例嗎？</font><font style="vertical-align: inherit;">-  Tracy Lee（This Dot）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:16:23</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320980.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">即將到來的隱私時代：勇敢和基本注意力令牌 -  Brendan Eich（勇敢的軟件）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:21:23</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320976.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">與Brendan Eich  -  Brendan Eich（Brave Software）的</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">問答環節00:10:56</font></font></span>
      
    
    
  </li>


      </ol>
    
  </li>

  <li class="toc-level-1 t-toc-level-1">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320896.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">贊助商</font></font></a>
      
    
    
      <ol>
        

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320896.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">衡量重要事項（由Akamai贊助） -  Cliff Crocker（Akamai）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:27:04</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320897.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">聊天機器人不只是為了遊戲（由SAP贊助） -  Meredith Hassett（SAP）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:38:33</font></font></span>
      
    
    
  </li>


      </ol>
    
  </li>

  <li class="toc-level-1 t-toc-level-1">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320899.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">框架和圖書館</font></font></a>
      
    
    
      <ol>
        

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320899.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Angular有什麼新東西 - 斯蒂芬·弗林（谷歌）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:39:00</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320900.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我在JavaScript地獄的一周 -  Keerthana Krishnan（Baker Hughes，GE公司）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:38:02</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320901.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">創建可重用的React組件庫 -  Cory House（Pluralsight | Cox Automotive）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:39:27</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320902.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何在Vue.js管理複雜狀態時保持理智 - 哈桑·吉爾德（Shopify）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:37:11</font></font></span>
      
    
    
  </li>


      </ol>
    
  </li>

  <li class="toc-level-1 t-toc-level-1">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320904.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Web服務和API</font></font></a>
      
    
    
      <ol>
        

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320904.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">區塊鏈作為後端：如何在您的應用程序中使用當前的加密貨幣 -  Destry Saul（Unchained Capital）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:23:25</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320905.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GraphQL的前端開發人員指南 -  Peggy Rayzis（Meteor Development Group）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:31:28</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320906.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用GraphQL訂閱的小組遊戲 - 亞歷克斯班克斯（Moon Highway）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:37:34</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320907.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">建立Alexa技能只是為了惹你的孩子--Bobby Johnson（由Auth0延伸）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:37:37</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320908.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Node.js服務的演變 - </font></font></a>
      
      <font style="vertical-align: inherit;"><span class="chapter-running-time"><font style="vertical-align: inherit;">GergelyNémeth </font></span><a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320908.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;">（GoDaddy）</font></a></font><span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:24:08</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320909.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是否需要成為PWA？</font><font style="vertical-align: inherit;">-  Tara Z. Manicsic（Progress）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:42:06</font></font></span>
      
    
    
  </li>


      </ol>
    
  </li>

  <li class="toc-level-1 t-toc-level-1">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320911.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">瀏覽器和前端工具</font></font></a>
      
    
    
      <ol>
        

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320911.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">10 KB或胸圍：webpack和Babel的微妙力量--Brian Holt（微軟）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:41:51</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320912.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Visual Studio Code可以做到嗎？</font><font style="vertical-align: inherit;">-  Burke Holland（微軟）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:36:12</font></font></span>
      
    
    
  </li>


      </ol>
    
  </li>

  <li class="toc-level-1 t-toc-level-1">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320914.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">性能和用戶體驗</font></font></a>
      
    
    
      <ol>
        

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320914.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">推，還是不推？</font><font style="vertical-align: inherit;">HTTP / 2服務器推送的未來--Patrick Hamann（Fastly）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:41:15</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320915.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">快攻的攻略：前端表演考古學 - 凱蒂·賽勒 - 米勒（Etsy）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:37:31</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320916.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下一個十億的實地測試接口 -  Ally Long（現場情報）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:44:42</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320917.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有意義的用戶體驗性能指標以及如何改進它們 - 馬克澤曼（SpeedCurve）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:44:35</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320918.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">黑客網絡性能 -  Maximiliano Firtman（ITMaster專業培訓）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:39:34</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320919.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">當第三方不再禮貌時。</font><font style="vertical-align: inherit;">。</font><font style="vertical-align: inherit;">並開始變得真實--Nic Jansma（Akamai），Charles Vazac（Akamai）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:39:39</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320920.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用HTTP存檔跟踪網絡性能 -  Paul </font></font></a>
      
      <font style="vertical-align: inherit;"><span class="chapter-running-time"><font style="vertical-align: inherit;">Calvano </font></span><a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320920.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;">（Akamai Technologies）</font></a></font><span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:38:21</font></font></span>
      
    
    
  </li>


      </ol>
    
  </li>

  <li class="toc-level-1 t-toc-level-1">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320922.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">未來的JS和功能</font></font></a>
      
    
    
      <ol>
        

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320922.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">足夠的功能編程對自己和同事構成危險 -  Kyle Shevlin（Formidable Labs）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:31:58</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320923.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">JavaScript模塊的未來 -  CJ Silverio（npm）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:40:12</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320924.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">什麼是WebAssembly有用？</font><font style="vertical-align: inherit;">-  Sasha Aickin（自僱）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:40:34</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320925.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">實踐中的TypeScript  -  Bryan Hughes（微軟）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:41:24</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320926.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">無服務器服務器端渲染：使用React和無服務器功能改善用戶體驗 -  Natalie Qabazard（Trulia）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:28:04</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320927.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">反應式編程：為什麼你應該關心以及如何編寫更多面向未來的代碼 -  Tracy Lee（This Dot）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:30:54</font></font></span>
      
    
    
  </li>


      </ol>
    
  </li>

  <li class="toc-level-1 t-toc-level-1">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320929.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">網絡業務</font></font></a>
      
    
    
      <ol>
        

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320929.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">意想不到的後果 - 金克雷頓（#causeascene）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:38:46</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320930.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">為藍領用戶構建軟件</font></font></a>
      
      <font style="vertical-align: inherit;"><span class="chapter-running-time"><font style="vertical-align: inherit;">--Wade </font></span><a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320930.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;">Minter（Custom Communications）</font></a></font><span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:39:44</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320931.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">開發人員需要關注許可證--Brian Rinaldi（Progress）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:38:52</font></font></span>
      
    
    
  </li>


      </ol>
    
  </li>

  <li class="toc-level-1 t-toc-level-1">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320933.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">移動和桌面</font></font></a>
      
    
    
      <ol>
        

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320933.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">思考PRPL  -  Houssein Djirdeh（Rangle.io）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:24:50</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320934.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">與Electron的跨平台桌面應用程序 -  David Neal（</font></font></a>
      
      <font style="vertical-align: inherit;"><span class="chapter-running-time"><font style="vertical-align: inherit;">ReverentGeek </font></span><a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320934.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;">）</font></a></font><span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:40:23</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320935.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自適應PWA：提供定制和優化的跨設備Web應用程序 -  Luis Vieira（Farfetch.com）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:27:52</font></font></span>
      
    
    
  </li>


      </ol>
    
  </li>

  <li class="toc-level-1 t-toc-level-1">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320937.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Web基礎：CSS，HTML，JS，Node</font></font></a>
      
    
    
      <ol>
        

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320937.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何在不失去靈魂的情況下構建實時應用程序 -  Matthew Larson（FamilySearch），Ian James（FamilySearch）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:30:09</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320938.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">.CSS {顯示：什麼？</font><font style="vertical-align: inherit;">-  Martine Dowden（Andromeda）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:31:32</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320939.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">這不是黑魔法：從你的樣式表中拉回窗簾 -  Aimee Knight（Built Technologies）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:40:22</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320940.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">EME？</font><font style="vertical-align: inherit;">CDM？</font><font style="vertical-align: inherit;">DRM？</font><font style="vertical-align: inherit;">CENC？</font><font style="vertical-align: inherit;">IDK！</font><font style="vertical-align: inherit;">-  Sebastian Golasch（Deutsche Telekom）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:40:37</font></font></span>
      
    
    
  </li>


      </ol>
    
  </li>

  <li class="toc-level-1 t-toc-level-1">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320942.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">架構和微服務</font></font></a>
      
    
    
      <ol>
        

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320942.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">無堆疊世界中的完整堆棧 - 托馬斯·布爾丁（谷歌），莎拉·艾倫（谷歌）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:38:59</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320944.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微前端介紹 -  Ivan Jovanovic（Welltok | nearForm）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:36:17</font></font></span>
      
    
    
  </li>


      </ol>
    
  </li>

  <li class="toc-level-1 t-toc-level-1">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320946.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安全</font></font></a>
      
    
    
      <ol>
        

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320946.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通過賦予每個人自身安全權來擁抱脆弱性 -  Annie Lau（Trulia）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:28:16</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320947.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Node.js漏洞中的模式 -  Chetan Karande（DTCC）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:35:47</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320948.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">秘密的藝術和工藝：使用加密工具箱 - 邁克爾斯威頓（原子對象）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:37:02</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320949.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">為隱私重建瀏覽器擴展 -  Princiya Sequeira（Zalando）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:38:19</font></font></span>
      
    
    
  </li>


      </ol>
    
  </li>

  <li class="toc-level-1 t-toc-level-1">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320951.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">無障礙</font></font></a>
      
    
    
      <ol>
        

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320951.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">包容性設計：讓人們重新聚焦 -  Sarah Federman（Adobe）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:30:58</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320952.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可訪問性很重要; </font><font style="vertical-align: inherit;">怎麼辦？</font><font style="vertical-align: inherit;">-  Juliana Gomez（巨大）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:40:25</font></font></span>
      
    
    
  </li>


      </ol>
    
  </li>

  <li class="toc-level-1 t-toc-level-1">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320954.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人和團隊</font></font></a>
      
    
    
      <ol>
        

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320954.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">成為團隊領導：生存指南 - 約瑟夫永利（SpeedCurve）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:38:22</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320955.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">現代工作流程：瞄準更快更好，不燃燒 -  Val Head（Adobe），Elaine Chao（Adobe）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:41:11</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320956.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">將設計思維應用於招聘：設計更好的招聘流程以優化喜悅和多樣性 -  Crystal Yan（美國數字服務）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:37:06</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321342.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">領導力始於傾聽：擴大影響力 -  Heidi Helfand（Procore Technologies）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:32:34</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320958.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">照顧你的同事 - 特倫特威利斯（Netflix）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:38:15</font></font></span>
      
    
    
  </li>


      </ol>
    
  </li>

  <li class="toc-level-1 t-toc-level-1">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320960.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">教程</font></font></a>
      
    
    
      <ol>
        

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320960.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">構建全球無服務器網站（由亞馬遜贊助）Shubham Katiyar（亞馬遜網絡服務）Sunil Chhablani（亞馬遜網絡服務）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:22:09</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320961.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可測試反應（第1部分） -  Pete Hodgson（獨立）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:35:13</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321283.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可測試反應（第2部分） -  Pete Hodgson（獨立）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:36:37</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321282.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可測試反應（第3部分） -  Pete Hodgson（獨立）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:52:59</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320962.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vue.js 101（第1部分） -  Benjamin Hong（Politico）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:34:31</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321286.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vue.js 101（第2部分） -  Benjamin Hong（Politico）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:30:04</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321285.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vue.js 101（第3部分） -  Benjamin Hong（Politico）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:31:45</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321284.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vue.js 101（第4部分） -  Benjamin Hong（Politico）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:31:56</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320963.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用合作創造用戶喜愛的產品（第1部分） -  Rachel Krause（尼爾森諾曼集團）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:37:27</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321287.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通過協作創造用戶喜愛的產品（第2部分） -  Rachel Krause（尼爾森諾曼集團）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:35:31</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320964.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Web Performance API深入探討（第1部分） -  Dan Shappir（Wix）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:35:08</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321290.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Web Performance API深入研究（第2部分） -  Dan Shappir（Wix）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:42:17</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321289.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Web Performance API深入探討（第3部分） -  Dan Shappir（Wix）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:32:49</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321288.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Web性能API深潛（第四部分） -丹Shappir（維克斯）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">0點35分五十○秒</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320965.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">讓您的移動網絡應用程序談話（第1部分） -  Scott Davis（ThoughtWorks）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:40:40</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321293.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">讓您的移動網絡應用程序談話（第2部分） -  Scott Davis（ThoughtWorks）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:24:02</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321292.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">讓您的移動網絡應用程序談話（第3部分） -  Scott Davis（ThoughtWorks）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:43:31</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321291.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">讓您的移動網絡應用程序談話（第4部分） -  Scott Davis（ThoughtWorks）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:40:46</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320966.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用Elm構建Web應用程序（第1部分） -  Jeremy Fairbank（測試雙人）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:37:48</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321296.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用Elm構建Web應用程序（第2部分） -  Jeremy Fairbank（測試雙倍）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:39:20</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321295.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用Elm構建Web應用程序（第3部分） -  Jeremy Fairbank（測試雙倍）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:29:23</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321294.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用Elm構建Web應用程序（第4部分） -  Jeremy Fairbank（測試雙倍）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:40:37</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320967.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">實用的動手可及性測試（第1部分） -  Nicolas Steenhout（整體的一部分）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:43:21</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321299.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">實用的動手可及性測試（第2部分） -  Nicolas Steenhout（整體的一部分）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:32:14</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321298.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">實用的動手可及性測試（第3部分） -  Nicolas Steenhout（整體的一部分）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:36:54</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321297.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">實用的動手可及性測試（第4部分） -  Nicolas Steenhout（整體的一部分）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:49:00</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320968.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通過構建遊戲來學習React（第1部分） -  Samer Buna（jsComplete）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:57:06</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321302.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通過構建遊戲來學習反應（第2部分） -  Samer Buna（jsComplete）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:49:00</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321301.html" class="t-chapter js-chapter">Learn React by building a game (Part 3) - Samer Buna (jsComplete)</a>
      
      <span class="chapter-running-time">00:36:35</span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321300.html" class="t-chapter js-chapter">Learn React by building a game (Part 4) - Samer Buna (jsComplete)</a>
      
      <span class="chapter-running-time">00:46:34</span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320970.html" class="t-chapter js-chapter">Reactive programming for frontend developers (Part 1) - Luca Mezzalira (DAZN)</a>
      
      <span class="chapter-running-time">00:44:23</span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321308.html" class="t-chapter js-chapter">Reactive programming for frontend developers (Part 2) - Luca Mezzalira (DAZN)</a>
      
      <span class="chapter-running-time">00:48:52</span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321307.html" class="t-chapter js-chapter">Reactive programming for frontend developers (Part 3) - Luca Mezzalira (DAZN)</a>
      
      <span class="chapter-running-time">00:39:46</span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321306.html" class="t-chapter js-chapter">Reactive programming for frontend developers (Part 4) - Luca Mezzalira (DAZN)</a>
      
      <span class="chapter-running-time">00:47:44</span>
      
    
    
  </li>


      </ol>
    
  </li>

  <li class="toc-level-1 t-toc-level-1">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320972.html" class="t-chapter js-chapter">Multiple Topics</a>
      
    
    
      <ol>
        

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320972.html" class="t-chapter js-chapter">Offline sync for progressive web apps - Bradley Holt (IBM Watson and Cloud Platform)</a>
      
      <span class="chapter-running-time">00:34:16</span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320973.html" class="t-chapter js-chapter">Discover the WebXR Device API (sponsored by Intel) - Alexis Menard (Intel)</a>
      
      <span class="chapter-running-time">00:35:13</span>
      
    
    
  </li>


      </ol>
    
  </li>

  <li class="toc-level-1 t-toc-level-1">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320978.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">閃電會談</font></font></a>
      
    
    
      <ol>
        

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320978.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">閃電會談 - 驚喜！</font><font style="vertical-align: inherit;">生成藝術的快樂</font></font></a>
      
      <font style="vertical-align: inherit;"><span class="chapter-running-time"><font style="vertical-align: inherit;">凱特康</font></span><a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video320978.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;">普頓（獨立）</font></a></font><span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:12:53</font></font></span>
      
    
    
  </li>

  <li class="toc-level-2 t-toc-level-2">
    
      <a href="https://www.safaribooksonline.com/library/view/fluent-conference-/9781492026037/video321310.html" class="t-chapter js-chapter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">閃電會談 - 使用您的API探索GraphQL Brian Douglas（GitHub）</font></font></a>
      
      <span class="chapter-running-time"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">00:11:32</font></font></span>
      
    
    
  </li>


      </ol>
    
  </li>


        </ol>